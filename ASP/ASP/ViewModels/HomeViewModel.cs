﻿using ASP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.ViewModels
{
    public class HomeViewModel
    {
        public IList<Course> Courses { get; set; }
    }
}
