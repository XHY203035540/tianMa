﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ASP.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Admins",
                columns: table => new
                {
                    Account = table.Column<string>(nullable: false),
                    Password = table.Column<string>(nullable: true),
                    IsUsable = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Admins", x => x.Account);
                });

            migrationBuilder.CreateTable(
                name: "Courses",
                columns: table => new
                {
                    CNO = table.Column<long>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    StartTime = table.Column<DateTime>(nullable: false),
                    Credit = table.Column<float>(nullable: false),
                    ClassHour = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    Academy = table.Column<string>(nullable: true),
                    IsAvailable = table.Column<bool>(nullable: false),
                    ImageURL = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Courses", x => x.CNO);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Admins");

            migrationBuilder.DropTable(
                name: "Courses");
        }
    }
}
