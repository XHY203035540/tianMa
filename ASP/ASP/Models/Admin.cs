﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.Models
{
    public class Admin
    {
        [Key]
        public string Account { get; set; }
        public string Password { get; set; }
        public bool IsUsable { get; set; }
    }
}
