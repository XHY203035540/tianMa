﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.Models
{
    public class DbInitializer
    {
        public static void Seed(MyDbContext myDbContext)
        {
            if (myDbContext.Courses.Any())
            {
                return;
            }
            myDbContext.AddRange
            (
                new Course { CNO = 1000, Name = "高等数学", Description = "超难！", StartTime = DateTime.Now, Credit = 5, ClassHour = 64, IsAvailable = true, Type = "必修课", Academy = "数统学院", ImageURL = "/images/1.jfif" },
                new Course { CNO = 1002, Name = "线性代数", Description = "超难！", StartTime = DateTime.Now, Credit = 5, ClassHour = 64, IsAvailable = true, Type = "必修课", Academy = "数统学院", ImageURL = "/images/2.webp" }
            );
            myDbContext.SaveChanges();
        }
    }
}
