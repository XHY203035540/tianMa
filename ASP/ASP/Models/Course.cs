﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.Models
{
    public class Course
    {
        [Key]
        public long CNO { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime StartTime { get; set; }
        public float Credit { get; set; }
        public int ClassHour { get; set; }
        public string Type { get; set; }
        public string Academy { get; set; }
        public Boolean IsAvailable { get; set; }
        public string ImageURL { get; set; }
    }
}
